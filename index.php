﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Green School Report</title>
<link href="css.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="js/jqwidgets/styles/jqx.base.css" type="text/css" />
	

	<script type="text/javascript" src="js/jqwidgets/jquery.min.js"></script>
	<script type="text/javascript" src="js/jqwidgets/jqxcore.js"></script>
	 <script type="text/javascript" src="js/jqwidgets/jqxdata.js"></script>
	 <script type="text/javascript" src="js/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxscrollbar.js"></script>
	<script type="text/javascript" src="js/jqwidgets/jqxmenu.js"></script>
	<script type="text/javascript" src="js/jqwidgets/jqxcheckbox.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.sort.js"></script> 
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.pager.js"></script> 
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.edit.js"></script> 
	<script type="text/javascript" src="js/jqwidgets/jqxgrid.columnsresize.js"></script> 
	<script type="text/javascript" src="js/jqwidgets/jqxdata.export.js"></script> 
   	<script type="text/javascript" src="js/jqwidgets/jqxgrid.export.js"></script> 
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="js/jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="js/jqwidgets/jqxpanel.js"></script>  
	<script type="text/javascript" src="js/jqwidgets/jqxgrid.grouping.js"></script>
	
	
</head>
<?php
		//$con = mssql_connect('192.168.99.106','linuxapps','linuxapps');

		if(!$con) {
    			die('Could not connect: ');
			  }
		else
				$db = mssql_select_db('gsm',$con);
	
	if($_GET['frm']==1)
	{
	 $query3 ='select a.SchoolCode,a.Name as SchoolName,b.VisitorName,a.Area,b.Location,a.visits,
	 a.VisitDate,a.CoversFilled,a.SeedsSowed,a.SaplingsCount,a.Availability,a.Cdate as CompletionDate  from SaplingDetails as a
				inner join tbl_School as b on a.SchoolCode= b.SchoolCode 
				group by a.SchoolCode,a.visits,a.Name,a.Area,a.VisitDate ,a.CoversFilled,a.SeedsSowed,a.SaplingsCount,a.Availability,b.Location,b.VisitorName,a.Cdate  order by a.SchoolCode ';
	}
	else
	{
	$query3 ='select a.SchoolCode,a.Name as SchoolName,b.VisitorName,a.Area,b.Location,a.visits,a.VisitDate,a.CoversFilled,
			  a.SeedsSowed,a.SaplingsCount,a.Availability,a.Cdate as CompletionDate from SaplingDetails as a,tbl_School as b 
			  where a.SchoolCode=b.SchoolCode and a.Sno in (select MAX(Sno) from SaplingDetails 
			  group by SchoolCode) order by a.SchoolCode' ;
		

	}
				
				$sqry3 = mssql_query($query3,$con);
				$retjson=array();
				$retjsoncol=array();
				
				$i=0; $j=0; $headcount =0;
 		  
				while($i < mssql_num_fields($sqry3)) 
			    { 	
					$row = array();
					$meta = mssql_fetch_field($sqry3);	
								
					$row['text']= $meta->name.$width;
					$row['datafield']=$meta->name;
					$retjsoncol[$i] = $row;
					$i++;

				} 
				while($field=mssql_fetch_array($sqry3))
					{
						$row = array();
						
						$row['SchoolCode'] = $field['SchoolCode'];
						$row['VisitorName'] = $field['VisitorName'];
						$row['SchoolName'] = $field['SchoolName'];
						$row['Area'] 		= $field['Area'];
						$row['Location'] 		= $field['Location'];
						$row['visits'] = $field['visits'];
						$row['VisitDate'] = date('d-m-Y',strtotime($field['VisitDate']));
						
						$row['CoversFilled'] = $field['CoversFilled'];
						$row['SeedsSowed'] = $field['SeedsSowed'];
						$row['SaplingsCount'] = $field['SaplingsCount'];
						if(strlen($field['CompletionDate'])==10)
						   $row['CompletionDate'] = date('d-m-Y',strtotime($field['CompletionDate']));
						  else  $row['CompletionDate']='NULL';
						
						if($field['Availability']=='Y') $stock='Yes';
						elseif($field['Availability']=='N') $stock='No';
						else $stock='Null';
						
						$row['Availability'] = $stock;
						
						$retjson[] = $row;
					}

					//header('Content-type: application/json');
					$result = array($retjsoncol,$retjson);
					$result = json_encode($result);
					
						?>


<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr>
    <td><table align="center" border="0" cellpadding="0" cellspacing="0" width="864">
      <tbody><tr>
        <td align="center" valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" valign="top" width="19"></td>
        <td bgcolor="#FFFFFF" valign="top" width="852">
            <table class="cbox" border="0" cellpadding="0" cellspacing="0" width="100%">
          <!--tr>
            <td>&nbsp;</td>
          </tr-->
          <tbody><tr>
            <td align="center"><img src="tracktree-header.jpg" height="151" width="852"></td>
          </tr>
		  		  <form name="pgh" action="" method="post"></form>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="track_ti">Green School <?php if($_GET['frm']<>1)  echo 'Summary '; else echo 'Detail' ?> Report</td>
          </tr>
          <tr>
            <td>
			<div style="margin-left: 680px;">
			<?php if($_GET['frm']<>1) {				 ?>			
			<a href="index.php?frm=1" style="color: green;">Show Detail Report</a> <?php } 
			else { ?>
			<a href="index.php?frm=2" style="color: green;">Show Summary Report</a> 	
			<?php } ?>
			</div></td> 
		
          </tr>
          <tr>
            <td><div class="widget">
     	<div class="widget-header">
        	<div class="title">
            	<span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span></div>
        </div>
        <div class="widget-body"> 
		<table align="center"><tr><td>
			<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left; display: block;" >		
				<div id="jqxgrid"></div>
		 		<div style="margin-top:10px;" >
				<div  style="float: left;"><input type="button" value="Export to Excel" id='excelExport' />
			
        		 </div></div>
     			</div>
			</td></tr></table>
			</div>
		</div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
		  
		   
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
        </tbody></table></td>
        <td align="center" valign="top" width="21"></td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>
<script>

		var source = {
					localdata: <?php echo $result; ?>[1] 
					};
			
				var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = '';
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtervalue = '';
                filtercondition = 'contains';
                var filter2 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtergroup.addfilter(filter_or_operator, filter1);
                filtergroup.addfilter(filter_or_operator, filter2);
                // add the filters.
                $("#jqxgrid").jqxGrid('addfilter', 'SchoolCode', filtergroup);
                // apply the filters.
                $("#jqxgrid").jqxGrid('applyfilters');
            }
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
				
                width: '800',
                source: dataAdapter,
                filterable: true,
                sortable: true,
                autoshowfiltericon: true,
                pageable: true, 
				pagesizeoptions : true,
				pagermode: 'simple',
                autoheight: true,
				columnsresize: true,
				groupable: true,
				editable: false,
				
				//groups: ['SchoolCode'],
				
                ready: function () {
                    addfilter();
                    var localizationObject = {
                        filterstringcomparisonoperators: ['contains', 'does not contain'],
                        // filter numeric comparison operators.
                        filternumericcomparisonoperators: ['less than', 'greater than'],
                        // filter date comparison operators.
                        filterdatecomparisonoperators: ['less than', 'greater than'],
                        // filter bool comparison operators.
                        filterbooleancomparisonoperators: ['equal', 'not equal']
                    }
                    $("#jqxgrid").jqxGrid('localizestrings', localizationObject);
                },
				
                updatefilterconditions: function (type, defaultconditions) {
                    var stringcomparisonoperators = ['CONTAINS', 'DOES_NOT_CONTAIN'];
                    var numericcomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var datecomparisonoperators = ['LESS_THAN', 'GREATER_THAN'];
                    var booleancomparisonoperators = ['EQUAL', 'NOT_EQUAL'];
                    switch (type) {
                        case 'stringfilter':
                            return stringcomparisonoperators;
                        case 'numericfilter':
                            return numericcomparisonoperators;
                        case 'datefilter':
                            return datecomparisonoperators;
                        case 'booleanfilter':
                            return booleancomparisonoperators;
                    }
                },
				
                updatefilterpanel: function (filtertypedropdown1, filtertypedropdown2, filteroperatordropdown, filterinputfield1, filterinputfield2, filterbutton, clearbutton,
                 columnfilter, filtertype, filterconditions) {
                    var index1 = 0;
                    var index2 = 0;
                    if (columnfilter != null) {
                        var filter1 = columnfilter.getfilterat(0);
                        var filter2 = columnfilter.getfilterat(1);
                        if (filter1) {
                            index1 = filterconditions.indexOf(filter1.comparisonoperator);
                            var value1 = filter1.filtervalue;
                            filterinputfield1.val(value1);
                        }
                        if (filter2) {
                            index2 = filterconditions.indexOf(filter2.comparisonoperator);
                            var value2 = filter2.filtervalue;
                            filterinputfield2.val(value2);
                        }
                    }
                    filtertypedropdown1.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index1 });
                    filtertypedropdown2.jqxDropDownList({ autoDropDownHeight: true, selectedIndex: index2 });
                },
				
               columns:<?php echo $result;?>[0],
			 
            });
		
			var width = 20 * $("#jqxgrid").width() / 100;
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SchoolCode', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'VisitorName', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SchoolName', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'Area', 'width', width);
		    $("#jqxgrid").jqxGrid('setcolumnproperty', 'visits', 'width',width);
			 $("#jqxgrid").jqxGrid('setcolumnproperty', 'Location', 'width',width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'VisitDate', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'CoversFilled', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SeedsSowed', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'CompletionDate', 'width', width);
			$("#jqxgrid").jqxGrid('setcolumnproperty', 'SaplingsCount', 'width', width); 
			/*$("#jqxgrid").jqxGrid('autoresizecolumns'); */
			
           $('#jqxGrid').jqxGrid({ pagesizeoptions: ['20', '40', '60']});
			//var pagesizeoptions = $('#jqxGrid').jqxGrid('pagesizeoptions'); 
		
			$("#jqxgrid").on("filter", function (event) {
            $("#events").jqxPanel('clearcontent');
            var filterinfo = $("#jqxgrid").jqxGrid('getfilterinformation');
            var eventData = "Triggered 'filter' event";
            	for (i = 0; i < filterinfo.length; i++) {
                 var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
                 $('#events').jqxPanel('prepend', '<div style="margin-top: 5px; ">' + eventData + '</div>');
                }
			
				$('#jqxbutton').click(function () {
				//$('#jqxgrid').jqxGrid('setcolumnproperty', 'SchoolCode','width',auto);
                $('#jqxgrid').jqxGrid('setcolumnproperty', 'visits', 'width', 200);
            });
				
            });
		
		
	$("#excelExport").click(function () {
		
		 var currentdate = new Date();
		 var currentdate1 = currentdate.getDate() + '-' + (currentdate.getMonth()+1) + '-' + currentdate.getFullYear();
		
		 var fname= 'Green_School_Movement_Report'+currentdate1;
    	$("#jqxgrid").jqxGrid('exportdata','xls', fname);           
	});	
			
	</script>

</body></html>